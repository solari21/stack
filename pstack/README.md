# Pstack
Pstack jest implementacją stosu w Pythonie.
## Technologie
* Python 3.7 i wyżej
* unittest
* pytest
## Uwagi techniczne
### Preferowane IDE
PyCharm
### pytest
Przed użyciem biblioteki pytest należy upewnić się co do wcześniejszej instalacji za pomocą modułu pip, np.: `py -m pip install pytest`.
### Uruchamianie testów
#### unittest
`py -m unittest tests/unittest_StackTest.py`
`py -m unittest tests/stack_tdd.py`

#### pytest
`py -m pytest tests/pytest_StackTest.py`

#### skryptem
./run_tests.sh

#### Docker
Jeśli korzystamy z Docker Toolbox to należy się upewnić, że katalog z projektem jest udostępniony w VirtualBox!

```
docker-compose up
docker-compose run --rm app ./run_tests.sh
```