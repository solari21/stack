# coding=utf-8
class Stack:
    # konstruktor obiektu stos inicjalizująca stack jako pustą tablicę, pointer i max ilość elementów stosu
    def __init__(self):
        self.stack = []
        self.pointer = 0
        self.max = 100

    #  metoda zwracająca kompletny stos
    def get_collection(self):
        return self.stack

    # metoda pop zdejmuje wierzchni element ze stosu
    def pop(self):
        self.pointer -= 1
        return self.stack.pop(self.pointer)

    # metoda push przyjmuje element dowolnego typu i umieszcza go w tablicy
    def push(self, item):
        self.stack.append(item)
        self.pointer += 1

    # metoda resetująca stos
    def reset(self):
        self.pointer = 0

    # metoda zwracająca ilość elementów na stosie
    def size(self):
        return self.pointer
